'use strict';

var iHost = "";
var iName = "";
var iRealName = "";
var iChannel = [];
var disChanID = "";
var disKey = "";

const irc = require('irc');
const iClient = new irc.Client(iHost, iName, {
    channels: iChannel,
    userName: iName.toLowerCase(),
    realName: iRealName,
    stripColors: true,
});

const Eris = require("eris");
const dis = new Eris(disKey);

dis.on("ready", () => {
    console.log('Discord OK!');
});

dis.on("messageCreate", (message) => {
    if (message.channel.id === disChanID && message.author.id !== dis.user.id) {
        iClient.say(iChannel, "<" + message.author.username + "> " + message.content);
    }
});

iClient.addListener('message', function (from, to, message) {
    var text = message;
    var regexp = /([`|_|@|*|\\])/g;
    var regexp_link = /(https?:\/\/\S+)/g;
    var re = new RegExp(regexp);

    if (re.test(text)) {
        if (from !== iName) dis.createMessage(disChanID, "<" + from + "> " + text.replace(regexp, "\\$1").replace(regexp_link, "<$1>") + "");
    } else {
        if (from !== iName) dis.createMessage(disChanID, "<" + from + "> " + text.replace(regexp_link, "<$1>") + "");
    }
});

dis.connect();